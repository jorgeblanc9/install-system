#################Instalar docker engine ####################
echo "Iniciando instalacion de docker engine"
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo service docker start
sudo groupadd docker
sudo gpasswd -a $USER docker
newgrp docker
docker run hello-world
echo "Instalacion de docker engine a finalizado"
echo "------------------------------------------"

#################Instalar docker Compose ####################
echo "Iniciando instalacion de docker compose"
sudo apt-get update
sudo apt-get install docker-compose-plugin
 docker compose version
echo "Instalacion de docker compose a finalizado"
echo "------------------------------------------"
